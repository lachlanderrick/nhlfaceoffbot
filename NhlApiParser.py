import urllib.request
from bs4 import BeautifulSoup
import pprint


class ApiAccessor:
    # Define constructor
    def __init__(self):

        # Get page containing all players on NHL team rosters
        self.main_page = urllib.request.urlopen(
            ('https://statsapi.web.nhl.com/api'
             '/v1/teams?expand=team.roster')).read()

        # Create required BeautifulSoup objects from page
        self.soup = BeautifulSoup(self.main_page)
        self.body = self.soup.find('body')
        self.body_without_tags = self.body.findChildren()

        # Create dictionary to store key (names) value (id) pairs
        self.players = dict()

    # Define parsing functions
    def trim_word(self, word):
        new_word = word.replace('"', '')
        new_word = new_word.replace(',', '')
        new_word = new_word.strip()
        return new_word

    # Set up dictionary mapping of names (key) to id (value)
    def create_player_dictionary(self):
        i = 0
        # NHL formats all it's data between one set of body tags
        split_body = self.body_without_tags[0].get_text().split()
        for _element in split_body:
            if (split_body[i] == '\"id\"' and
                    split_body[i+3] == '\"fullName\"'):

                # Add player name and corresponding id to dictionary
                first_name = self.trim_word(split_body[i+5])
                second_name = self.trim_word(split_body[i+6])
                player_id = self.trim_word(split_body[i+2])

                self.players[first_name + " " + second_name] = player_id
            i += 1
        return self.players

    # Create player object with seasonal stats
    def addPlayerData(self, player_id, player_name):
        details_page = urllib.request.urlopen(
            ('https://statsapi.web.nhl.com/api/v1/people/' + player_id +
             '/stats?stats=statsSingleSeason'))

        # Create required BeautifulSoup objects from page
        soup = BeautifulSoup(details_page)
        body = soup.find('body')
        body_without_tags = body.findChildren()

        # Create player dictionary to store player stats
        player = dict()
        player['NAME'] = player_name

        # Find and add all stats
        i = 0
        # NHL formats all it's data between one set of body tags
        split_body = body_without_tags[0].get_text().split()
        for _element in split_body:
            if split_body[i] == '\"timeOnIce\"':
                player['TOI'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"assists\"':
                player['A'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"goals\"':
                player['G'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"pim\"':
                player['PIM'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"shots\"':
                player['S'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"games\"':
                player['GP'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"hits\"':
                player['HITS'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"powerPlayGoals"':
                player['PPG'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"powerPlayTimeOnIce\"':
                player['PP TOI'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"penaltyMinutes\"':
                player['PIM'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"faceOffPct\"':
                player['FO%'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"shotPct\"':
                player['S%'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"gameWinningGoals\"':
                player['GWG'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"overTimeGoals\"':
                player['OTG'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"shortHandedGoals\"':
                player['SHG'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"plusMinus\"':
                player['+/-'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"points\"':
                player['P'] = self.trim_word(split_body[i+2])
            elif split_body[i] == '\"timeOnIcePerGame\"':
                player['PG TOI'] = self.trim_word(split_body[i+2])

            i += 1
        return player
