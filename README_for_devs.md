# NHLFaceoffBot
===================

NHLFaceoffBot is a Python Reddit bot that compares two NHL players using up to date player statistics.

Installation
===================

Requirements:
* Python 3.6
* [PRAW](https://praw.readthedocs.io/en/latest/)
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)
* [Tabulate](https://pypi.org/project/tabulate/)


Clone repository to desired local location.

Usage
===================

To run: python3 NHLFaceoffBot.py 

To change which subreddits the bot is active in, change the value of the 'subreddit' variable in the NHLFaceoffBot.py file. Multiple subreddits can also be used.

Contributing
==================

Pull requests welcome, open an issue to discuss prospective changes / features.
