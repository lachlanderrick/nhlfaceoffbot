# HockeyBot

import praw
from tabulate import tabulate
from NhlApiParser import ApiAccessor

# Create required reddit objects from imported module

# Reddit login information
reddit = praw.Reddit(client_id='DZo3ql2eRyTlIQ',
                     client_secret='1H4tn9ukJD32W-5NrHZv_VcIaYk',
                     username='NHLFaceoffBot',
                     password='NHLFaceoffBotPass',
                     user_agent='NHLFaceoffBot by u/postie_')

# Subreddit bot is active in
subreddit = reddit.subreddit('postie_BotTesting')

# Phrase used to call bot
keyphrase = '!NHLFaceoffBot'

# Instantiate object used to access Api
accessor = ApiAccessor()

# Instantiate player dictionary
players = accessor.create_player_dictionary()

# Set up table, make list of lists from values in player dictionaries


def makeTable(player_one, playerTwo):

    values = [list(player_one.values()), list(player_two.values())]

    table = tabulate(values, headers=player_one.keys(), tablefmt="pipe")

    return table


# Check for keyphrase and reply upon bot call
for comment in subreddit.stream.comments(skip_existing=True):
    if keyphrase in comment.body:
        if 'vs' in comment.body:
            try:
                split_comment = comment.body.split()
                player_one_name = split_comment[1] + " " + split_comment[2]
                player_two_name = split_comment[4] + " " + split_comment[5]

                player_one_id = players[player_one_name]
                player_two_id = players[player_two_name]

                player_one = accessor.addPlayerData(
                    player_one_id, player_one_name)
                player_two = accessor.addPlayerData(
                    player_two_id, player_two_name)

                reply_comment = makeTable(player_one, player_two)

            except KeyError:
                reply_comment = ("Unfortunately I couldn't find "
                                 "a player you asked about.")

        reply_comment = reply_comment + \
            ("\n\n***\n\n ^(Beep boop, I'm a bot. Need help? Try [here.]"
             "(https://github.com/lachlanderrick/NHLFaceoffBot/blob/master"
             "/README_for_users.md))")

        comment.reply(reply_comment)
        print("posted")
