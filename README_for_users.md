# NHLFaceoffBot
===================

NHLFaceoffBot is a Python Reddit bot that compares two NHL players using up to date player statistics.

Usage
===================

Summoning:

Use the keyphrase, !NHLFaceoffBot in your reddit comment to summon NHLFaceoffBot. Even if you don't ask him to compare any players, he still might say hello to you!

Comparison:

To compare players, post a comment (prefaced by the above keyphrase), formatted as follows:

player_one_name vs player_two_name

Player names must be spelled correctly for NHLFaceoffBot to find their statistics.

Feedback
==================

If you have any feedback, please send reddit user u/postie_ a message. He would love to hear your thoughts!
